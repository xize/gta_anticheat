# GTA ANTI CHEAT

### What is GTA Anti cheat?
this program scans for malicious packets, packets who don't fall for
certain requirements and are marked invalid.

often these packets belong to modders or people who got infected by a modder
who have spawned entities or vehicles inside the session.


### How does it work?
Often when people play gta online the port of incomming packets is always 6672.
this program aims to scan on this port on the external traffic, since gta online uses UDP peer to peer it needs to use hole punching in order to synchronize with peers and it uses udp checksums to correspond with the packet lengths and the container size inside it.
if one peer invalidates it will kick him out from the others.

however some mod menus fail to follow the path of networking as how it is intended by R* and sent invalid
packets with illegal container sizes, this means it might be a modder but it could also be somebody using some kind of infection like from a modder contanimated session earlier with vehicles or entities, the player will sent the same kind of modded traffic across other sessions and for this reason we use the jail approach.

so what happens when our program detects a malicious packet?

our program adds the ip in our firewall rule, and adds a jail time and this person will often be kicked out of the session, however if he was the host you will be kicked regardless.

### How accurate is this anti cheat?
this program can detect some modders but not all modders, from alot of testing some modders can be detected but not all sent this kind of traffic.
also its not cool to brag about this tool in question, since according the EULA from rockstargames network manipulation is highly bannable.

We are not responsible or liable if your account gets banned or other damages are occuried by the use of our software.

### Dependencies you need to get this program working:

install winpcap https://www.winpcap.org/ or try installing npcap (untested)

### Licenses and Libraries
our program is written in c# and our source code is GPLv3 and made by Guido Lucassen

We use the following libraries:
- YamlDotNet https://github.com/aaubry/YamlDotNet
- PacketDotNet https://github.com/dotpcap/packetnet
- SharpPcap https://github.com/dotpcap/sharppcap
- WinPcap https://www.winpcap.org/
- NPcap https://github.com/nmap/npcap
