﻿using gta_anticheat.controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gta_anticheat
{
    public partial class Window : Form
    {

        private PacketScanner pscanner;

        public Window()
        {
            InitializeComponent();
            this.pscanner = new PacketScanner();
        }

        private void Window_Load(object sender, EventArgs e)
        {
            this.moveAbleWindow1.ControlTarget = new Control[] { this.titlebarpanel,this.logo };
            this.dropdownControl1.DropDownChangeEvent += DropDown;
            this.Config = new Configuration();

            
        }

        public Configuration Config
        {
            get;private set;
        }

        private void DropDown(object sender, DropDownArguments e)
        {
            this.label4.Text = e.GetValue();
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void panel2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DeviceSelectionWindow dwin = new DeviceSelectionWindow();
            DialogResult r = dwin.ShowDialog();
            if(r == DialogResult.OK)
            {
                Config cfg = Configuration.GetMainConfiguration.GetConfig;
                cfg.StandardDevice = dwin.GetDeviceName;
                cfg.JAIL_TIME = this.label4.Text;

                Configuration.GetMainConfiguration.SaveConfig(cfg);
            }
        }

        private void Window_FormClosing(object sender, FormClosingEventArgs e)
        {
            Config cfg = Configuration.GetMainConfiguration.GetConfig;
            Configuration.GetMainConfiguration.SaveConfig(cfg);
        }

        private void antiCheatStatusControl1_AntiCheatActivateEvent(object sender, EventArgs e)
        {
            if(this.antiCheatStatusControl1.Activate)
            {
                pscanner.ScanGTAPacket();
            } else
            {
                pscanner.StopScan.Abort();
            }
        }
    }
}
