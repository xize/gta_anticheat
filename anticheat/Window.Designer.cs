﻿namespace gta_anticheat
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.titlebarpanel = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.logo = new System.Windows.Forms.Panel();
            this.trevorart = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.statsspacer = new System.Windows.Forms.Panel();
            this.stats = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dropdownControl1 = new gta_anticheat.controls.DropdownControl();
            this.antiCheatStatusControl1 = new gta_anticheat.controls.AntiCheatStatusControl();
            this.moveAbleWindow1 = new gta_anticheat.controls.MoveAbleWindow();
            this.roundedWindow1 = new gta_anticheat.controls.RoundedWindow();
            this.titlebarpanel.SuspendLayout();
            this.trevorart.SuspendLayout();
            this.stats.SuspendLayout();
            this.SuspendLayout();
            // 
            // titlebarpanel
            // 
            this.titlebarpanel.Controls.Add(this.panel2);
            this.titlebarpanel.Controls.Add(this.panel1);
            this.titlebarpanel.Controls.Add(this.logo);
            this.titlebarpanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.titlebarpanel.Location = new System.Drawing.Point(0, 0);
            this.titlebarpanel.Name = "titlebarpanel";
            this.titlebarpanel.Size = new System.Drawing.Size(200, 23);
            this.titlebarpanel.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::gta_anticheat.Properties.Resources.minimizebtn;
            this.panel2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel2.Location = new System.Drawing.Point(163, 9);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(10);
            this.panel2.Size = new System.Drawing.Size(16, 9);
            this.panel2.TabIndex = 2;
            this.panel2.Click += new System.EventHandler(this.panel2_Click);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::gta_anticheat.Properties.Resources.closebtn;
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Location = new System.Drawing.Point(182, 9);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(8, 9);
            this.panel1.TabIndex = 1;
            this.panel1.Click += new System.EventHandler(this.panel1_Click);
            // 
            // logo
            // 
            this.logo.BackgroundImage = global::gta_anticheat.Properties.Resources.logo;
            this.logo.Dock = System.Windows.Forms.DockStyle.Left;
            this.logo.Location = new System.Drawing.Point(0, 0);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(91, 23);
            this.logo.TabIndex = 1;
            // 
            // trevorart
            // 
            this.trevorart.BackgroundImage = global::gta_anticheat.Properties.Resources.centerart;
            this.trevorart.Controls.Add(this.button1);
            this.trevorart.Location = new System.Drawing.Point(0, 68);
            this.trevorart.Name = "trevorart";
            this.trevorart.Size = new System.Drawing.Size(200, 240);
            this.trevorart.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Impact", 8F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(10, 101);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(178, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Select Network Adapter";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // statsspacer
            // 
            this.statsspacer.BackgroundImage = global::gta_anticheat.Properties.Resources.sats_spacer;
            this.statsspacer.Dock = System.Windows.Forms.DockStyle.Top;
            this.statsspacer.Location = new System.Drawing.Point(0, 47);
            this.statsspacer.Name = "statsspacer";
            this.statsspacer.Size = new System.Drawing.Size(200, 2);
            this.statsspacer.TabIndex = 2;
            // 
            // stats
            // 
            this.stats.BackgroundImage = global::gta_anticheat.Properties.Resources.statsbg;
            this.stats.Controls.Add(this.label4);
            this.stats.Controls.Add(this.label3);
            this.stats.Controls.Add(this.label2);
            this.stats.Controls.Add(this.label1);
            this.stats.Dock = System.Windows.Forms.DockStyle.Top;
            this.stats.Location = new System.Drawing.Point(0, 23);
            this.stats.Name = "stats";
            this.stats.Size = new System.Drawing.Size(200, 24);
            this.stats.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Impact", 8F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.label4.Location = new System.Drawing.Point(163, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "30M";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Impact", 8F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.label3.Location = new System.Drawing.Point(119, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Jail Time:";
            // 
            // label2
            // 
            this.label2.AutoEllipsis = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Impact", 8F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.label2.Location = new System.Drawing.Point(89, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "1231345";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Impact", 8F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.label1.Location = new System.Drawing.Point(13, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Currently jailed:";
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = global::gta_anticheat.Properties.Resources.control_left_style;
            this.panel3.Location = new System.Drawing.Point(0, 48);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(53, 20);
            this.panel3.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = global::gta_anticheat.Properties.Resources.control_right_style;
            this.panel4.Location = new System.Drawing.Point(155, 49);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(45, 19);
            this.panel4.TabIndex = 1;
            // 
            // dropdownControl1
            // 
            this.dropdownControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dropdownControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dropdownControl1.BackgroundImage")));
            this.dropdownControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.dropdownControl1.Collapse = false;
            this.dropdownControl1.Location = new System.Drawing.Point(53, 49);
            this.dropdownControl1.Name = "dropdownControl1";
            this.dropdownControl1.Size = new System.Drawing.Size(102, 19);
            this.dropdownControl1.TabIndex = 0;
            // 
            // antiCheatStatusControl1
            // 
            this.antiCheatStatusControl1.Activate = false;
            this.antiCheatStatusControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("antiCheatStatusControl1.BackgroundImage")));
            this.antiCheatStatusControl1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.antiCheatStatusControl1.Location = new System.Drawing.Point(0, 308);
            this.antiCheatStatusControl1.Name = "antiCheatStatusControl1";
            this.antiCheatStatusControl1.Size = new System.Drawing.Size(200, 171);
            this.antiCheatStatusControl1.TabIndex = 5;
            this.antiCheatStatusControl1.AntiCheatActivateEvent += new System.EventHandler<System.EventArgs>(this.antiCheatStatusControl1_AntiCheatActivateEvent);
            // 
            // moveAbleWindow1
            // 
            this.moveAbleWindow1.ControlTarget = null;
            this.moveAbleWindow1.MainWindowTarget = this;
            // 
            // roundedWindow1
            // 
            this.roundedWindow1.Border = 8;
            this.roundedWindow1.BorderColor = System.Drawing.Color.Transparent;
            this.roundedWindow1.Radius = 10;
            this.roundedWindow1.TargetControl = this;
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(49)))), ((int)(((byte)(49)))));
            this.ClientSize = new System.Drawing.Size(200, 480);
            this.Controls.Add(this.dropdownControl1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.antiCheatStatusControl1);
            this.Controls.Add(this.statsspacer);
            this.Controls.Add(this.stats);
            this.Controls.Add(this.titlebarpanel);
            this.Controls.Add(this.trevorart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(0, 9);
            this.Name = "Window";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gta Anti cheat";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Window_FormClosing);
            this.Load += new System.EventHandler(this.Window_Load);
            this.titlebarpanel.ResumeLayout(false);
            this.trevorart.ResumeLayout(false);
            this.stats.ResumeLayout(false);
            this.stats.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel titlebarpanel;
        private System.Windows.Forms.Panel logo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel stats;
        private System.Windows.Forms.Panel statsspacer;
        private System.Windows.Forms.Panel trevorart;
        private controls.MoveAbleWindow moveAbleWindow1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private controls.RoundedWindow roundedWindow1;
        private controls.AntiCheatStatusControl antiCheatStatusControl1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private controls.DropdownControl dropdownControl1;
        private System.Windows.Forms.Button button1;
    }
}

