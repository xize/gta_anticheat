﻿using SharpPcap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gta_anticheat
{
    public partial class DeviceSelectionWindow : Form
    {
        private PacketScanner scanner = new PacketScanner();

        public DeviceSelectionWindow()
        {
            InitializeComponent();
        }

        public string GetDeviceName
        {
            get;private set;
        }

        private void DeviceSelectionWindow_Load(object sender, EventArgs e)
        {
            string[] devices = scanner.ScanInterfaces();
            if (devices.Count() < 1)
            {
                MessageBox.Show("no network devices are found!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else
            {
                foreach (string device in devices)
                {
                    listBox1.Items.Add(device);
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {

            if(this.listBox1.SelectedIndex < 0)
            {
                MessageBox.Show("please select a adapter first!", "error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            ILiveDevice dev = scanner.GetDeviceByDescription(this.listBox1.Text);

            GetDeviceName = dev.Name;

            this.Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

    }
}
