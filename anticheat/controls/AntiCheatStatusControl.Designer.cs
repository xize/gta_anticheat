﻿namespace gta_anticheat.controls
{
    partial class AntiCheatStatusControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // AntiCheatStatusControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::gta_anticheat.Properties.Resources.av_off;
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Name = "AntiCheatStatusControl";
            this.Size = new System.Drawing.Size(200, 171);
            this.Click += new System.EventHandler(this.AntiCheatStatusControl_Click);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
