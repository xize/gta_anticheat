﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gta_anticheat.controls
{
    public partial class DropdownControl : UserControl
    {
        public DropdownControl()
        {
            InitializeComponent();
        }

        private bool collapsed = false;
        public event EventHandler<DropDownArguments> DropDownChangeEvent;

        public bool Collapse
        {
            get
            {
                return this.collapsed;
            }
            set
            {
                if(value)
                {
                    this.Height = 130;
                } else
                {
                    this.Height = 19;
                }
                this.Update();
                this.collapsed = value;
            }
        }

        private void dropdownright_Click(object sender, EventArgs e)
        {
            this.Collapse = !this.Collapse;

        }

        private void DropdownControl_Leave(object sender, EventArgs e)
        {
            if(this.Collapse)
                this.Collapse = !this.Collapse;
        }

        private void panel1_Click(object sender, EventArgs e)
        {
            this.dropdownlabel.Text = "30M";
            this.Collapse = !this.Collapse;

            EventHandler<DropDownArguments> handler = DropDownChangeEvent;
            if(handler != null)
            {
                DropDownArguments args = new DropDownArguments("30M");
                handler?.Invoke(this, args);
            }
            
        }

        private void panel2_Click(object sender, EventArgs e)
        {
            this.dropdownlabel.Text = "60M";
            this.Collapse = !this.Collapse;

            EventHandler<DropDownArguments> handler = DropDownChangeEvent;
            if (handler != null)
            {
                DropDownArguments args = new DropDownArguments("60M");
                handler?.Invoke(this, args);
            }
        }

        private void panel3_Click(object sender, EventArgs e)
        {
            this.dropdownlabel.Text = "1D";
            this.Collapse = !this.Collapse;

            EventHandler<DropDownArguments> handler = DropDownChangeEvent;
            if (handler != null)
            {
                DropDownArguments args = new DropDownArguments("1D");
                handler?.Invoke(this, args);
            }
        }

        private void panel5_Click(object sender, EventArgs e)
        {
            this.dropdownlabel.Text = "3D";
            this.Collapse = !this.Collapse;

            EventHandler<DropDownArguments> handler = DropDownChangeEvent;
            if (handler != null)
            {
                DropDownArguments args = new DropDownArguments("3D");
                handler?.Invoke(this, args);
            }
        }
    }

    public class DropDownArguments : EventArgs
    {

        private string newval;

        public DropDownArguments(string newval)
        {
            this.newval = newval;
        }

        public string GetValue()
        {
            return this.newval;
        }

    }
}
