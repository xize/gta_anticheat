﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gta_anticheat.controls
{
    public partial class AntiCheatStatusControl : UserControl
    {
        public AntiCheatStatusControl()
        {
            InitializeComponent();
        }


        public event EventHandler<EventArgs> AntiCheatActivateEvent;

        private bool activated = false;

        public bool Activate
        {
            get
            {
                return this.activated;
            }
            set
            {
                if (value)
                    this.BackgroundImage = gta_anticheat.Properties.Resources.av_on;
                else
                    this.BackgroundImage = gta_anticheat.Properties.Resources.av_off;

                this.activated = value;
                this.Update();

                EventHandler<EventArgs> handler = AntiCheatActivateEvent;
                if(handler != null)
                {
                    handler?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        private void AntiCheatStatusControl_Click(object sender, EventArgs e)
        {
            this.Activate = !this.Activate;
        }
    }
}
