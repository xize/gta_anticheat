﻿namespace gta_anticheat.controls
{
    partial class DropdownControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.dropdownbtncontainer = new System.Windows.Forms.Panel();
            this.dropdownright = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dropdownlabel = new System.Windows.Forms.Label();
            this.dropdownleft = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.dropdownbtncontainer.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Impact", 7F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(125)))), ((int)(((byte)(125)))));
            this.label2.Location = new System.Drawing.Point(1, 2);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Set Jail:";
            // 
            // dropdownbtncontainer
            // 
            this.dropdownbtncontainer.Controls.Add(this.dropdownright);
            this.dropdownbtncontainer.Controls.Add(this.panel4);
            this.dropdownbtncontainer.Controls.Add(this.dropdownleft);
            this.dropdownbtncontainer.Location = new System.Drawing.Point(43, 3);
            this.dropdownbtncontainer.Name = "dropdownbtncontainer";
            this.dropdownbtncontainer.Size = new System.Drawing.Size(54, 12);
            this.dropdownbtncontainer.TabIndex = 3;
            // 
            // dropdownright
            // 
            this.dropdownright.BackgroundImage = global::gta_anticheat.Properties.Resources.jailcontrol_anchor_symbol;
            this.dropdownright.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dropdownright.Dock = System.Windows.Forms.DockStyle.Left;
            this.dropdownright.Location = new System.Drawing.Point(38, 0);
            this.dropdownright.Name = "dropdownright";
            this.dropdownright.Size = new System.Drawing.Size(17, 12);
            this.dropdownright.TabIndex = 5;
            this.dropdownright.Click += new System.EventHandler(this.dropdownright_Click);
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = global::gta_anticheat.Properties.Resources.jailcontrol_inputbg;
            this.panel4.Controls.Add(this.dropdownlabel);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(5, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(33, 12);
            this.panel4.TabIndex = 4;
            // 
            // dropdownlabel
            // 
            this.dropdownlabel.BackColor = System.Drawing.Color.Transparent;
            this.dropdownlabel.Font = new System.Drawing.Font("Impact", 8F);
            this.dropdownlabel.ForeColor = System.Drawing.Color.White;
            this.dropdownlabel.Location = new System.Drawing.Point(-2, -3);
            this.dropdownlabel.Name = "dropdownlabel";
            this.dropdownlabel.Size = new System.Drawing.Size(35, 19);
            this.dropdownlabel.TabIndex = 0;
            this.dropdownlabel.Text = "30M";
            this.dropdownlabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dropdownleft
            // 
            this.dropdownleft.BackgroundImage = global::gta_anticheat.Properties.Resources.jailcontrol_corner;
            this.dropdownleft.Dock = System.Windows.Forms.DockStyle.Left;
            this.dropdownleft.Location = new System.Drawing.Point(0, 0);
            this.dropdownleft.Name = "dropdownleft";
            this.dropdownleft.Size = new System.Drawing.Size(5, 12);
            this.dropdownleft.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Location = new System.Drawing.Point(4, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(93, 19);
            this.panel1.TabIndex = 4;
            this.panel1.Click += new System.EventHandler(this.panel1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Impact", 8F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(32, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "30M";
            this.label1.Click += new System.EventHandler(this.panel1_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel2.Controls.Add(this.label3);
            this.panel2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel2.Location = new System.Drawing.Point(4, 51);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(93, 19);
            this.panel2.TabIndex = 5;
            this.panel2.Click += new System.EventHandler(this.panel2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Impact", 8F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(32, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "60M";
            this.label3.Click += new System.EventHandler(this.panel2_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.label4);
            this.panel3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel3.Location = new System.Drawing.Point(4, 76);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(93, 19);
            this.panel3.TabIndex = 6;
            this.panel3.Click += new System.EventHandler(this.panel3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Impact", 8F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(37, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "1D";
            this.label4.Click += new System.EventHandler(this.panel3_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.label5);
            this.panel5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel5.Location = new System.Drawing.Point(4, 101);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(93, 19);
            this.panel5.TabIndex = 7;
            this.panel5.Click += new System.EventHandler(this.panel5_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Impact", 8F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(37, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 15);
            this.label5.TabIndex = 0;
            this.label5.Text = "3D";
            this.label5.Click += new System.EventHandler(this.panel5_Click);
            // 
            // DropdownControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.BackgroundImage = global::gta_anticheat.Properties.Resources.jailcontrol_bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dropdownbtncontainer);
            this.Controls.Add(this.label2);
            this.Name = "DropdownControl";
            this.Size = new System.Drawing.Size(102, 19);
            this.Leave += new System.EventHandler(this.DropdownControl_Leave);
            this.dropdownbtncontainer.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel dropdownbtncontainer;
        private System.Windows.Forms.Panel dropdownright;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label dropdownlabel;
        private System.Windows.Forms.Panel dropdownleft;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label5;
    }
}
