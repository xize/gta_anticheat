﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace gta_anticheat
{
    public class Configuration
    {

        private string WORK_DIR
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + Path.DirectorySeparatorChar + "Gta Anti Cheat" + Path.DirectorySeparatorChar;
            }
        }

        public Configuration(){
            Load();
            GetMainConfiguration = this;
        }

        public void Load()
        {
            this.CreateDirectories();

            if (!File.Exists(WORK_DIR + "config.yml")) {
                DeviceSelectionWindow dwin = new DeviceSelectionWindow();
                DialogResult r = dwin.ShowDialog();

                if (r == DialogResult.Cancel)
                {
                    Application.Exit();
                    return;
                }

                Config cfg = new Config();
                cfg.StandardDevice = dwin.GetDeviceName;
                cfg.JAIL_TIME = "30M";
                cfg.SQL_DB_FILE = "anticheat.sqlite";
                
                var serializer = new SerializerBuilder().Build();
                string yamltext = serializer.Serialize(cfg);

                File.WriteAllText(WORK_DIR + "config.yml", yamltext);

                GetConfig = cfg;

            } else
            {
                var deserializer = new DeserializerBuilder().Build();
                string yamlinput = File.ReadAllText(WORK_DIR + "config.yml");
                GetConfig = deserializer.Deserialize<Config>(yamlinput);
            }

        }

        public void SaveConfig(Config ccfg)
        {
            Config cfg = ccfg;

            var serializer = new SerializerBuilder().Build();
            string text = serializer.Serialize(cfg);

            File.WriteAllText(WORK_DIR + "config.yml", text);
        }

        public Config GetConfig
        {
            get;private set;
        }

        private void CreateDirectories()
        {

            if (!Directory.Exists(WORK_DIR))
                Directory.CreateDirectory(WORK_DIR);
        }

        public static Configuration GetMainConfiguration
        {
            get; private set;
        }

    }

    public class Config
    {
        public string StandardDevice { get; set; }

        public string JAIL_TIME { get; set; }
        public string SQL_DB_FILE { get; set; }
    }
}
