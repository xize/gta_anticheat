﻿using PacketDotNet;
using SharpPcap;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gta_anticheat
{
    public class PacketScanner
    {

        public PacketScanner()
        {

        }

        public string[] ScanInterfaces()
        {
            string[] devicenames = new string[0];

            try
            {
                CaptureDeviceList deviceslist = CaptureDeviceList.Instance;
                if (deviceslist.Count < 0)
                    return null;

                devicenames = new string[deviceslist.Count];

                int index = 0;

                foreach (ILiveDevice device in deviceslist)
                {
                    devicenames[index] += device.Description;
                    index++;
                }
            }
            catch (DllNotFoundException)
            {
                MessageBox.Show("could not find WinPcap please install this!", "generic error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Process.Start("explorer", @"https://www.winpcap.org/install/default.htm");
                Environment.Exit(0);
            }

            return devicenames;
        }

        public ILiveDevice GetDeviceByDescription(string desc)
        {
            try
            {
                CaptureDeviceList deviceslist = CaptureDeviceList.Instance;
                if (deviceslist.Count < 0)
                    return null;

                foreach (ILiveDevice device in deviceslist)
                {
                    if (device.Description == desc)
                        return device;
                }
            }
            catch (DllNotFoundException)
            {
                MessageBox.Show("could not find WinPcap please install this!", "generic error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Process.Start("explorer", @"https://www.winpcap.org/install/default.htm");
                Environment.Exit(0);
            }
            return null;
        }

        public ILiveDevice GetLiveDeviceByName(string name)
        {
            CaptureDeviceList deviceslist = CaptureDeviceList.Instance;
            foreach (ILiveDevice dev in deviceslist)
            {
                if (dev.Name == name)
                    return dev;
            }
            return null;
        }

        public Thread StopScan
        {
            get; private set;
        }

        public void ScanGTAPacket()
        {

            ThreadStart th = new ThreadStart(() =>
            {
                Config cfg = Configuration.GetMainConfiguration.GetConfig;
                ILiveDevice dev = GetLiveDeviceByName(cfg.StandardDevice);

                dev.Open(DeviceModes.NoCaptureLocal);
                dev.Filter = "udp && dst port 6672";

                while (true)
                {
                    PacketCapture capture;

                    while (((GetPacketStatus)dev.GetNextPacket(out capture)) != GetPacketStatus.NoRemainingPackets)
                    {

                        if (capture.GetPacket() != null && capture.GetPacket().GetPacket() != null && capture.GetPacket().GetPacket() is Packet)
                        {
                            try
                            {
                                ulong a = (ulong)capture.GetPacket().GetPacket().Bytes.Length;
                                ulong b = (ulong)capture.GetPacket().GetPacket().HeaderData.Length;


                                if (a - b > 0)
                                {
                                    IPPacket ip = capture.GetPacket().GetPacket().Extract<IPPacket>();
                                    MessageBox.Show("Test 1 triggered (packet body - header = ? > 0 ): offender found: " + ip.SourceAddress.ToString());
                                }


                                UdpPacket u = capture.GetPacket().GetPacket().Extract<UdpPacket>();

                                if (u is UdpPacket)
                                {
                                    UdpPacket upacket = u;

                                    if (!upacket.IsValidChecksum(TransportPacket.TransportChecksumOption.None))
                                    {
                                        IPPacket ip = capture.GetPacket().GetPacket().Extract<IPPacket>();
                                        MessageBox.Show("Test 2 triggered (IsValidChecksum): offender found: " + ip.SourceAddress.ToString());
                                    }
                                    else if (!upacket.ValidChecksum)
                                    {
                                        IPPacket ip = capture.GetPacket().GetPacket().Extract<IPPacket>();
                                        MessageBox.Show("Test 2 triggered (ValidChecksum): offender found: " + ip.SourceAddress.ToString());
                                    }
                                    else if (!upacket.ValidUdpChecksum)
                                    {
                                        IPPacket ip = capture.GetPacket().GetPacket().Extract<IPPacket>();
                                        MessageBox.Show("Test 2 triggered (ValidUdpChecksum): offender found: " + ip.SourceAddress.ToString());
                                    }
                                }
                            }
                            catch (IndexOutOfRangeException) { }
                        }
                    }

                }
            });
            Thread t = new Thread(th);
            StopScan = t;
            t.Start();

        }

    }
}
